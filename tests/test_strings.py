import unittest
import pytest
import pytest_check as check
import json
import re

from pathlib import Path
from typing import Generator, Tuple


folder = Path(__file__).parents[1] / "arma_terrain_validator"  # type: Path
strings_file = folder / "l8n" / "en_gb.json"  # type: Path


class TestStrings(unittest.TestCase):
    def test_paths_valid(self):
        assert folder.exists(), "Incorrect folder path in test_strings.py"
        assert strings_file.exists(), "Incorrect l8n path in test_strings.py"

    def test_l8n_strings(self):
        self.load_strings()
        for string, path in self.find_gettext():
            check.is_true(self.gettext_is_valid(string), self.format_string(string, path))

    def load_strings(self):
        with strings_file.open(mode="r") as fp:
            self.strings = json.load(fp)

    @staticmethod
    def format_string(string: str, path: Path):
        filename = str(path.relative_to(folder))
        return f"'{string}' is missing in l8n\nUsed in: {filename}"

    def find_gettext(self) -> Generator[Tuple[str, Path], None, None]:
        """Finds all the gettext usage within the module"""
        reg_ex = "gettext\([\"'](?P<string>.+?)[\"'][\)\.]"
        for file in folder.glob("**/*.py"):
            with file.open(mode="r") as fp:
                results = re.findall(reg_ex, fp.read())
                for result in results:
                    yield result, file

    def gettext_is_valid(self, string: str):
        return string in self.strings.keys()
