from collections import OrderedDict
from pathlib import Path
from typing import List, Union
from arma_terrain_validator.armaclass import parse
from arma_terrain_validator.libs import lower_dict, dict_find_key
from arma_terrain_validator.libs.strings import gettext

import pytest
import unittest
import json

folder = Path(__file__).parent / "data"


class TestArmaClass(unittest.TestCase):
    def test_macros(self):
        with (folder / "config.cpp").open() as fp:
            read = fp.read()
            data = parse(read)
        assert data is not None
