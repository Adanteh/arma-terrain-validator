class CfgSurfaces {
    class Default {};
    class Water {};
    class frl_sagrass: Default {
        access = 2;
        files = "frl_sagrass*";
        character = "frlSaGrassclutter";
        soundEnviron = "grass_exp";
        soundHit = "soft_ground";
        rough = 0.1;
        maxSpeedCoef = 1;
        dust = 0.5;
        lucidity = 100;
        grassCover = 0.3;
        impact = "hitGroundSoft";
        surfaceFriction = 1.2;
        maxClutterColoringCoef = 10;
        tracksAlpha = 0.8;
    };

    class frl_saforest: Default {
        access = 2;
        files = "frl_saforest*";
        character = "frlSaForestclutter";
        soundEnviron = "grass_exp";
        soundHit = "soft_ground";
        rough = 0.6;
        maxSpeedCoef = 0.7;
        dust = 0.1;
        lucidity = 100;
        grassCover = 0.8;
        impact = "hitGroundSoft";
        surfaceFriction = 1.2;
        maxClutterColoringCoef = 4;
        tracksAlpha = 0.6;
    };

    class frl_coast: Default	{
        access = 2;
        files = "frl_coast*";
        character = "frlSaCoastclutter";
        soundEnviron = "seabed_exp";
        soundHit = "hard_ground";
        rough = 0.6;
        maxSpeedCoef = 0.85;
        dust = 0.75;
        lucidity = 0.5;
        grassCover = 0.05;
        maxClutterColoringCoef = 2;
        impact = "hitGroundHard";
        surfaceFriction = 1.2;
        tracksAlpha = 1;
    };

    class frl_sa_asphalt: Default   {
        access = 2;
        files = "frl_sa_asphalt*";
        character = "empty";
        soundEnviron = "concrete";
        soundHit = "concrete";
        rough = 0.05;
        maxSpeedCoef = 1;
        dust = 0.05;
        lucidity = 0.3;
        grassCover = 0;
        impact = "hitConcrete";
        maxClutterColoringCoef = 1;
        tracksAlpha = 0.7;
    };

    class frl_sacliff: Default {
        access = 2;
        files = "frl_sacliff*";
        character = "empty";
        soundEnviron = "rock";
        soundHit = "hard_ground";
        rough = 0.8;
        maxSpeedCoef = 0.8;
        dust = 0.5;
        lucidity = 1;
        grassCover = 0;
        impact = "hitGroundHard";
        surfaceFriction = 1.8;
        maxClutterColoringCoef = 2;
        tracksAlpha = 0;
    };

    class frl_sadirt: Default {
        access = 2;
        files = "frl_sadirt*";
        character = "frlSaDirtclutter";
        soundEnviron = "grass_exp";
        soundHit = "soft_ground";
        rough = 0.6;
        maxSpeedCoef = 0.7;
        dust = 1;
        lucidity = 100;
        grassCover = 0.4;
        impact = "hitGroundSoft";
        surfaceFriction = 1.2;
        maxClutterColoringCoef = 4;
        tracksAlpha = 1;
    };
};

class CfgSurfaceCharacters {
    class frlSaCoastclutter {
        probability[] = {0.1};
        names[] =       {"frl_sa_small_stones"};
    };
    class frlSaGrassclutter {
        probability[] = {0.12,													0.15,												0.1,											0.1};
        names[] =       {"frl_sa_grass_short_small",    "frl_sa_strgrassgreengroup",    "frl_sa_volcano_grassmix",  "frl_sa_grass_short_leaf"};
    };
    class frlSaForestclutter {
        probability[] = {0.3,													0.25,												0.2,											0.05};
        names[] =       {"frl_sa_grass_short_small",    "frl_sa_strgrassgreengroup",    "frl_sa_volcano_grassmix",  "frl_sa_grass_short_leaf"};
    };
    class frlSaDirtclutter {
        probability[] = {0.01};
        names[] =       {"frl_sa_grass_short_leaf"};
    };
};
