class CfgPatches {
    class frl_saaremaa {
        requiredAddons[] = {
            "A3_Data_F",
            "A3_Roads_F",
            "A3_Structures_F",
            "A3_Map_Data",
            "A3_Map_Stratis",
            "frl_saaremaa_detail"
        };
        requiredVersion = 0.1;
        units[] = {};
        weapons[] = {};
    };
};

class CfgWorlds  {
    class DefaultWorld {
        class Weather {
            class Overcast;
        };
        class WaterExPars;
    };

    class CAWorld: DefaultWorld {
        class Grid;
        class DayLightingBrightAlmost;
        class DayLightingRainy;
        class DefaultClutter;
        class EnvSounds;
        class Weather: Weather {
            class Lighting;
            class Overcast: Overcast {
                class Weather1;
                class Weather2;
                class Weather3;
                class Weather4;
                class Weather5;
                class Weather6;
            };
        };
    };

    class DefaultLighting;
    class frl_saaremaa : CAWorld {
        #include "cfgClutter.h"

        clutterGrid = 1;
        clutterDist = 70;
        noDetailDist = 150;
        fullDetailDist = 5;
        clutterRoadwayCheckRadiusCoef = "0.5f";
        interpolateClutterColoring = 1;
        clutterColoringFarCoef = "8.0f";
        clutterColoringFarStart = "1.0f";
        clutterColoringFarSpeed = "1.0f";
        maxClutterColoringCoef = "3.0f";
    };
    initWorld = "frl_saaremaa";
};

#include "cfgSurfaces.h"

class CfgWorldList {
    class frl_saaremaa{};
};
