class clutter {
	class frl_sa_StrGrassGreenGroup: DefaultClutter {
		model = "A3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
		affectedByWind = 0;
		swLighting = 1;
		scaleMin = 0.7;
		scaleMax = 1;
	};
	class frl_sa_Grass_short_small: DefaultClutter {
		model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_small.p3d";
		affectedByWind = 0; // 0.1;
		swLighting = 1;
		scaleMin = 0.7;
		scaleMax = 1.1;
	};
	class frl_sa_Grass_short_leaf: DefaultClutter {
		model = "A3\Vegetation_F_Exp\Clutter\Grass\c_Grass_short_leaf.p3d";
		affectedByWind = 0; // 0.1;
		swLighting = 1;
		scaleMin = 1;
		scaleMax = 1.6;
	};
	class frl_sa_small_stones: DefaultClutter {
		model = "A3\Vegetation_F_Exp\Clutter\Red_dirt\c_red_dirt_small_stones.p3d";
		affectedByWind = 0; // 0;
		swLighting = 1;
		scaleMin = 0.3;
		scaleMax = 0.85;
	};
	class frl_sa_large_stones: DefaultClutter {
		model = "A3\Vegetation_F_Exp\Clutter\Red_dirt\c_red_dirt_large_stones.p3d";
		affectedByWind = 0; // 0;
		swLighting = 1;
		scaleMin = 0.8;
		scaleMax = 1;
	};
	class frl_sa_volcano_GrassMix: DefaultClutter {
		model = "A3\Vegetation_F_Exp\Clutter\Volcano\c_volcano_GrassMix.p3d";
		affectedByWind = 0; // 0.05;
		swLighting = 0;
		scaleMin = 0.7;
		scaleMax = 1;
	};
};
