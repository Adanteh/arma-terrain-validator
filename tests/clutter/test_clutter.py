import pytest
import unittest
from collections import OrderedDict

from pathlib import Path
from arma_terrain_validator.clutter import ConfigCollector, ClutterException

test_data = Path(__file__).parents[1] / "data"  # type: Path


class ClutterTest(unittest.TestCase):
    def test_layers_load(self):
        surfaces = ConfigCollector.from_file(test_data / "cfgSurfaces.h")
        self.assertIsInstance(surfaces['cfgsurfaces'], OrderedDict)

    def test_assert_missing(self):
        surfaces = ConfigCollector.from_file(test_data / "cfgSurfaces.h")
        with self.assertRaises(ClutterException):
            surfaces = ConfigCollector.from_file(test_data / "cfgSurfaces.h")
            surfaces.run()

    def test_wrong_file(self):
        with self.assertRaises(FileNotFoundError):
            ConfigCollector.from_file(test_data / "nonExistant.h")

    def test_valid(self):
        surfaces = ConfigCollector.from_file(test_data / "cfgSurfaces.h")
        surfaces.add_file(test_data / 'cfgClutter.h')
        surfaces.run()
