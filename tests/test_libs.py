import unittest
import pytest
from collections import OrderedDict

from arma_terrain_validator.libs import lower_dict, dict_find_key, dict_find_keys


class LibTest(unittest.TestCase):
    def test_rename_keys_lower(self):
        values = dict(Upper=1, Nested=dict(Key=2), List=[3])
        result = lower_dict(values)
        expected = dict(upper=1, nested=dict(key=2), list=[3])
        self.assertDictEqual(expected, result)

        # Checks ordered dict
        order_values = OrderedDict(Upper=1, Nested=OrderedDict(Key=2), List=[3])
        order_result = lower_dict(order_values)
        order_expected = OrderedDict(upper=1, nested=OrderedDict(key=2), list=[3])
        self.assertEqual(order_expected, order_result)

        depth_expect = dict(upper=1, nested=dict(Key=2), list=[3])
        depth_result = lower_dict(values, max_level=1)
        self.assertDictEqual(depth_expect, depth_result)

    def test_find_key(self):
        values = dict(Upper=1, Nested=dict(Key=2), List=[3])
        assert dict_find_key(values, "Key") == 2, "Nested search"
        assert (
            dict_find_key(values, "key", case_insensitive=True) == 2
        ), "Case insensitive expected success"
        assert (
            dict_find_key(values, "key", case_insensitive=False) == None
        ), "Case sensitive expected fail"
        assert dict_find_key(values, "invalid") == None, "Non-existant key"

    def test_find_keys(self):
        values = dict(Upper=1, Nested=dict(Key=2), List=[3])
        assert dict_find_keys(values, ("Key",)) == {"Key": 2}, "Nested search"
        assert dict_find_keys(values, ("key",)) == {"key": None}, "Case sensitive expected fail"
        assert dict_find_keys(values, ("key",), case_insensitive=True) == {
            "key": 2
        }, "Case sensitive expected find"
        assert dict_find_keys(values, ("Upper", "invalid")) == {
            "Upper": 1,
            "invalid": None,
        }, "Mixed results"
