from collections import OrderedDict
from pathlib import Path
from typing import List, Union, Tuple
from arma_terrain_validator.armaclass import parse
from arma_terrain_validator.libs import lower_dict, dict_find_key, dict_find_keys
from arma_terrain_validator.libs.strings import gettext


CHARACTER_BLACKLIST = ("empty",)


class ClutterException(Exception):
    pass


def load_file(path: Path, lower=True) -> OrderedDict:
    if not path.exists():
        raise FileNotFoundError(str(path))

    with path.open(mode="r") as fp:
        values = parse(fp.read(), keep_order=True)
    if lower:
        return lower_dict(values)
    else:
        return values


class ConfigCollector:
    """
        Allows you to pass in multiple different configs, combining them together
        and tests if all required classes are valid, before we can start our operation
    """

    REQUIRED = ("CfgSurfaces", "CfgSurfaceCharacters", "Clutter")

    def __init__(self):
        self.classes = {key.lower(): None for key in self.REQUIRED}

    @classmethod
    def from_file(cls, path: Path):
        """Creates the object and instantly loads a file to it"""
        obj = cls()
        obj.add_file(path)
        return obj

    def add_file(self, path: Path):
        """Adds a config file to our collector. If it contains one of the required classes, save it for further usage
        
        Args:
            path (Path): Path to the config file
        """
        file = load_file(path, lower=True)
        result = dict_find_keys(file, self.REQUIRED, case_insensitive=True)
        for key, value in result.items():
            if value:
                self.classes[key] = value

    def run(self):
        if self.check_valid():
            validator = SurfaceValidator(*tuple(self[key.lower()] for key in self.REQUIRED))
            for name, surface in validator:
                validator.check_surface(name, surface)

    def __getitem__(self, key):
        return self.classes[key]

    def __setitem__(self, key, value):
        self.classes[key] = value

    def check_valid(self):
        if not self.valid:
            raise ClutterException(gettext("MISSING_CLASSES".format(", ".join(self.missing))))
        return True

    @property
    def valid(self) -> bool:
        """All surfaces have been detected properly"""
        return all(self.classes.values())

    @property
    def missing(self) -> Tuple[str]:
        """Returns missing required classes names"""
        return tuple(filter(lambda key: self.classes[key] is None, self.classes.keys()))


class SurfaceValidator:
    """
        This validates a CfgSurfaces entry
        A surface refers to which textures to use, which properties the ground has (dust, roughness, etc)
        Plus which character (collection of clutter entries) is used for that ground type

        For example: Sand, with sandy texture, an empty character (no plants) and a lot of dust
    """

    def __init__(
        self,
        surfaces: OrderedDict = None,
        characters: OrderedDict = None,
        clutter: OrderedDict = None,
    ):
        self.characters = characters
        self.surfaces = surfaces
        self.clutter = clutter

    def __iter__(self):
        for key, value in self.surfaces.items():
            if value:
                yield key, value

    def check_surface(self, key: str, surface: OrderedDict) -> bool:
        """Main function to check if a given surface is valid
        
        Args:
            surface (OrderedDict): The class dictionary
            key (str): The class name
        
        Returns:
            bool: True if valid surface (Files, character, clutter elements, etc)
        """

        char_name = surface.get("character", None)
        if char_name is None:
            raise ClutterException(gettext("MISSING_CHARACTER").format(key))

        if not self.check_character_valid(char_name):
            return False

        return True

    def check_character_valid(self, char_name: str) -> bool:
        """Checks if the given character is valid
        
        Args:
            char_name (str): Refers to the CfgSurfaceCharacters class
        
        Returns:
            bool: True if character is valid
        """
        if char_name in CHARACTER_BLACKLIST:
            return True

        characters = self.characters
        char = characters.get(char_name.lower(), None)
        if char is None:
            raise ClutterException(gettext("MISSING_CHARACTER_CLASS").format(char_name))

        character = CharacterValidator(char_name, char, self.clutter)
        character.check()
        return True


class CharacterValidator:
    """
        The class to check if a CfgSurfaceCharacter is valid
        A character refers to the COLLECTION of different clutter models and their probabilities

        For example: Forest, has mostly ferns, some branches, and an mixed flowers
    """

    def __init__(self, name: str, char_class: OrderedDict, clutter: OrderedDict):
        self.name = name
        self.char_class = char_class
        self.clutter = clutter
        self.__checked_names = []  # type: List[str]

    def check(self):
        """Does full check for current cfgSurfaceCharacters class"""
        probabilities = self.check_probabilities()
        names = self.check_names()

        len_prob = len(probabilities)
        len_names = len(names)
        if len_prob != len_names:
            raise ClutterException(gettext("MISMATCH_LENGTH").format(self.name))

    def check_probabilities(self) -> List[float]:
        """Checks if probablities entry is set up properly
        
        Returns:
            List[float]: Probabilities list
        """
        probabilities = self.char_class.get("probability", None)
        if probabilities is None:
            raise ClutterException(gettext("MISSING_PROBABILITY").format(self.name))

        if sum(probabilities) > 1.0:
            raise ClutterException(gettext("PROBABILITY_EXCEED_ONE").format(self.name))

        return probabilities

    def check_names(self) -> List[str]:
        """Checks the class entries named in Names entry
        
        Returns:
            List[str]: Names for the clutter classes
        """
        names = self.char_class.get("names", None)
        if names is None:
            raise ClutterException(gettext("MISSING_NAMES").format(self.name))

        for name in names:
            self.check_name(name)

        return names

    def check_name(self, name: str, case_insensitive=True):
        """
            Checks one of the name entries for this character
            Each name refers to a Clutter class, which is a path to model, with min and max sizes
            These should be present within the CfgWorlds entry for a given terrain
        """

        if case_insensitive:
            name = name.lower()

        if name in self.__checked_names:
            return None

        clutter = self.clutter.get(name, None)
        if clutter is None:
            raise ClutterException(gettext("MISSING_CLUTTER").format(name))

        return True
