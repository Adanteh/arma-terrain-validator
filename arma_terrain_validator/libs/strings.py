from pathlib import Path
import json

source = Path(__file__).parents[1] / "l8n" / "en_gb.json"
with source.open() as fp:
    STRINGS = json.load(fp)  # type: dict


def gettext(var: str) -> str:
    try:
        return STRINGS[var]
    except KeyError:
        print(f"[WARNING]: Missing l8n string: '{var}'")
        return var
