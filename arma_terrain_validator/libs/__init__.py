from typing import Union, Iterable, Tuple, Any, Dict
from collections import OrderedDict


def dict_change_key(obj: Iterable, convert: callable, max_level=0, _level=1):
    """Recursively modifies each key of a dictionary with the convert function, works recursively
    
    Args:
        obj (Iterable): Object to modify, should be key, value type
        convert (callable): Function to call over the key, should return string
        max_level (int, Optional): Maximum depth for recursion (1 being root level)
    """

    if max_level and _level > max_level:
        return obj

    if isinstance(obj, (str, int, float)):
        return obj
    if isinstance(obj, (dict, OrderedDict)):
        new = obj.__class__()
        for k, v in obj.items():
            new[convert(k)] = dict_change_key(v, convert, max_level, _level + 1)
    elif isinstance(obj, (list, set, tuple)):
        new = obj.__class__(dict_change_key(v, convert, max_level, _level + 1) for v in obj)
    else:
        return obj
    return new


def dict_find_key(obj: dict, find: str, case_insensitive=False) -> Union[None, Any]:
    """Recursive dict iterator, searching for a key, 
    
    Args:
        obj (dict): The dictionary to search recursively
    """
    if case_insensitive:
        find = find.lower()

    for key, value in obj.items():
        if case_insensitive:
            key = key.lower()
        if key == find:
            return value

        if isinstance(value, dict):
            result = dict_find_key(value, find, case_insensitive=case_insensitive)
            if result is not None:
                return result

    return None


def dict_find_keys(
    obj: dict, find: Tuple[str], case_insensitive=False, results=None
) -> Dict[str, Any]:
    """
        Recursive dict iterator, searching for multiple keys, returns a dict with found results
        This will traverse through the entire dictionary, till all requested keys have been found
    
    Args:
        obj (dict): The dictionary to search recursively
    """
    if case_insensitive:
        find = tuple(key.lower() for key in find)

    if results is None:
        results = {key: None for key in find}

    for key, value in obj.items():
        if case_insensitive:
            key = key.lower()
        if key in find:
            results[key] = value

        if all(results.values()):
            return results

        if isinstance(value, dict):
            dict_find_keys(value, find, case_insensitive=case_insensitive, results=results)

    return results


def lower_dict(obj: Iterable, **kwargs):
    """Lowers a dictionary
    
    Args:
        obj (Iterable): Items to lower
        max_level (int, Optional): Maximum depth for recursion (1 being root level)
    """
    return dict_change_key(obj, lambda x: x.lower(), **kwargs)
